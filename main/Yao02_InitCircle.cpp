/*
 * Yao02_InitCircle.cpp
 *
 *  Created on: Apr 19, 2015
 *      Author: yao
 */

#include "DataLoader.h"
#include <boost/lexical_cast.hpp>
#include "ExplicitActiveContour.h"

using namespace std;
using namespace activecontour;

int main(int argc, char *argv[]) {
  string dataDir = "/home/yao/Data/snakes/testimages/";
  string imageFilenamePre = "test4";
  string imageFilenameExt = ".jpg";

  DataLoader loader(dataDir, imageFilenamePre, imageFilenameExt, 90);
  loader.loadImage();

  ExplicitActiveContour activeContour;
  activeContour.setInputImage(loader.getOriginalImage());
  activeContour.setOutputDir("/home/yao/Data/snakes/iterations/");

  cv::Point center(120, 160);
  double deltaP = activeContour.initialize(center, 40, 10);
  activeContour.runActiveContour(100, 0.1, deltaP, 0.3);
  return 0;
}


