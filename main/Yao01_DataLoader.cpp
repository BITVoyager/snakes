/*
 * Yao01_DataLoader.cpp
 *
 *  Created on: Apr 6, 2015
 *      Author: yao
 */

#include "DataLoader.h"

#include <boost/lexical_cast.hpp>

using namespace std;
using namespace activecontour;

int main(int argc, char *argv[]) {
  string dataDir = "/home/yao/Data/snakes/testimages/";
  string imageFilenamePre = "test1";
  string imageFilenameExt = ".jpg";
  size_t nrIterations = 500;
  double timeInterval = 0.1;
  double lambda = 4;
  double gamma = 0.3;

  // parsing parameters
  if (argc == 5) {
    nrIterations = boost::lexical_cast<size_t>(argv[1]);
    timeInterval = boost::lexical_cast<double>(argv[2]);
    lambda = boost::lexical_cast<double>(argv[3]);
    gamma = boost::lexical_cast<double>(argv[4]);
  }

  DataLoader loader(dataDir, imageFilenamePre, imageFilenameExt, 90);
  loader.loadImage();
  //loader.blurImageHeatEquation(500, 0.1);
  loader.blurImageNonlinearDiff(nrIterations, timeInterval, lambda, gamma);
  loader.saveEnergy("/home/yao/Data/snakes/testimages/txt/energy.txt");
  return 0;
}


