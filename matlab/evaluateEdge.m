close all;
clear all;
clc

inputDir = '/home/yao/Data/snakes/testimages/blurredImage/';
outputDir = '/home/yao/Data/snakes/testimages/edgeImage/';
imagesDir = dir(inputDir);

for k = 3:size(imagesDir, 1)
    filename = fullfile(inputDir, imagesDir(k).name);
    image = rgb2gray(imread(filename));
    bw = edge(image);
    outputFilename = strcat(outputDir, 'edge-', imagesDir(k).name);
    imwrite(bw, outputFilename);
end