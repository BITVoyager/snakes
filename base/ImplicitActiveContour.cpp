/*
 * ImplicitActiveContour.cpp
 *
 *  Created on: Apr 22, 2015
 *      Author: yao
 */

#include "ImplicitActiveContour.h"

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <boost/filesystem.hpp>
#include <boost/lexical_cast.hpp>

#include <vector>

#include <iostream>
#include <fstream>

namespace activecontour {

/// create a circle as the initial input
size_t ImplicitActiveContour::initialize(cv::Point center, double radius, int resolution) {
  std::cout << "[Level Set] Starting initializing..." << std::endl;
  // convert the radius from double to int
  double difference = radius - floor(radius);
  int intRadius = floor(radius);

  // assign points to the zero level set
  cv::Size axes(intRadius, intRadius);
  if (resolution < 2) {
    std::cout << "[Level Set] Resolution is wrong!" << std::endl;
    exit(-1);
  }
  cv::ellipse2Poly(center, axes, 0, 0, 360, resolution, zeroLevelSet_);
  zeroLevelSet_.pop_back();  // remove the last element, which is the same with the first one

  // assign points to other places of psi
  inputImage_.copyTo(psi_);
  inputImage_.copyTo(nearestIndex_);
  nearestIndex_.convertTo(nearestIndex_, CV_16U);
  for (int r = 0; r < nearestIndex_.rows; r++) {
    for (int c = 0; c < nearestIndex_.cols; c++) {
      // find the shortest distance
      double shortestDist = 1e10;
      for (int k = 0; k < zeroLevelSet_.size(); k++) {
        cv::Point currentPosition = zeroLevelSet_.at(k);
        double currentDist = cv::sqrt(cv::pow(c - currentPosition.x, 2) + cv::pow(r - currentPosition.y, 2));
        if (currentDist < shortestDist) {
          if (currentDist == 0.0)
            currentDist = difference;
          shortestDist = currentDist;
          nearestIndex_.at<uint16_t>(r, c) = k;
        }
      }

      // assign initial values
      double distToCirCenter = cv::sqrt(cv::pow(c - center.x, 2) + cv::pow(r - center.y, 2));
      if (distToCirCenter < radius) {
        psi_.at<double>(r, c) = - shortestDist;
      } else if (distToCirCenter > radius) {
        psi_.at<double>(r, c) = shortestDist;
      } else {
        std::cout << "[Level Set] Initialization is wrong, please check!" << std::endl;
        exit(-1);
      }
    }
  }

  // save image;
  saveImage();

  // next iteration
  currentIter_++;

  std::cout << "[Level Set] Initialization ends!" << std::endl;
  return zeroLevelSet_.size();
}

/// the function Phi(x)
void ImplicitActiveContour::PhiDescription(double lambda) {
  // compute gradients
  cv::Mat Ix;
  cv::Mat Iy;
  computeImageGradient(Ix, Iy, 3); // central difference

  Phi_ = cv::pow(lambda, 2) / (Ix.mul(Ix) + Iy.mul(Iy) + cv::pow(lambda, 2));
  cv::Mat kernelX = (cv::Mat_<double>(1, 3) << -0.5, 0, 0.5);
  cv::Mat kernelY = (cv::Mat_<double>(3, 1) << -0.5, 0, 0.5);
  cv::filter2D(Phi_, PhiX_, -1, kernelX, cv::Point(-1, -1), 0.02, cv::BORDER_REFLECT);
  cv::filter2D(Phi_, PhiY_, -1, kernelY, cv::Point(-1, -1), 0.02, cv::BORDER_REFLECT);

}

/// compute gradients of the original Image
void ImplicitActiveContour::computeImageGradient(cv::Mat& Ix, cv::Mat& Iy, int mode) {
  cv::Mat kernelX;
  cv::Mat kernelY;

  if (mode == 1) {  // forward difference
    kernelX = (cv::Mat_<double>(1, 2) << -1, 1);
    kernelY = (cv::Mat_<double>(2, 1) << -1, 1);
    cv::filter2D(inputImage_, Ix, -1, kernelX, cv::Point(0, 0), 0, cv::BORDER_REFLECT);
    cv::filter2D(inputImage_, Iy, -1, kernelY, cv::Point(0, 0), 0, cv::BORDER_REFLECT);
  } else if (mode == 2) {  // backward difference
    kernelX = (cv::Mat_<double>(1, 2) << -1, 1);
    kernelY = (cv::Mat_<double>(2, 1) << -1, 1);
    cv::filter2D(inputImage_, Ix, -1, kernelX, cv::Point(1, 0), 0, cv::BORDER_REFLECT);
    cv::filter2D(inputImage_, Iy, -1, kernelY, cv::Point(0, 1), 0, cv::BORDER_REFLECT);
  } else if (mode == 3) {  // first order central difference
    kernelX = (cv::Mat_<double>(1, 3) << -0.5, 0, 0.5);
    kernelY = (cv::Mat_<double>(3, 1) << -0.5, 0, 0.5);
    cv::filter2D(inputImage_, Ix, -1, kernelX, cv::Point(-1, -1), 0, cv::BORDER_REFLECT);
    cv::filter2D(inputImage_, Iy, -1, kernelY, cv::Point(-1, -1), 0, cv::BORDER_REFLECT);
  } else if (mode == 4) {  // second order central difference
    kernelX = (cv::Mat_<double>(1, 3) << 1, -2, 1);
    kernelY = (cv::Mat_<double>(3, 1) << 1, -2, 1);
    cv::filter2D(inputImage_, Ix, -1, kernelX, cv::Point(-1, -1), 0, cv::BORDER_REFLECT);
    cv::filter2D(inputImage_, Iy, -1, kernelY, cv::Point(-1, -1), 0, cv::BORDER_REFLECT);
  } else {
    std::cout << "[Level set] Mode is wrong!";
  }
}

/// extend Phi and gradient of Phi
void ImplicitActiveContour::PhiExtension() {
  std::cout << "[Level Set] Computing extended Phi..." << std::endl;

  // extension of Phi
  inputImage_.copyTo(extendedPhi_);
  inputImage_.copyTo(extendedPhiGradientX_);
  inputImage_.copyTo(extendedPhiGradientY_);
  for (int r = 0; r < extendedPhi_.rows; r++) {
    for (int c = 0; c < extendedPhi_.cols; c++) {
      // compute extended Phi
      int index = nearestIndex_.at<uint16_t>(r, c);
      int x = zeroLevelSet_.at(index).x;
      int y = zeroLevelSet_.at(index).y;
      extendedPhi_.at<double>(r, c) = Phi_.at<double>(y, x);
      extendedPhiGradientX_.at<double>(r, c) = PhiX_.at<double>(y, x);
      extendedPhiGradientY_.at<double>(r, c) = PhiY_.at<double>(y, x);
    }
   }
  std::cout << "[Level Set] Computing extended Phi ends!" << std::endl;
}

/// compute gradients of the original Image
void ImplicitActiveContour::computePsiGradient(cv::Mat& Ix, cv::Mat& Iy, int mode) {
  cv::Mat kernelX;
  cv::Mat kernelY;

  if (mode == 1) {  // forward difference
    kernelX = (cv::Mat_<double>(1, 2) << -1, 1);
    kernelY = (cv::Mat_<double>(2, 1) << -1, 1);
    cv::filter2D(psi_, Ix, -1, kernelX, cv::Point(0, 0), 0, cv::BORDER_REFLECT);
    cv::filter2D(psi_, Iy, -1, kernelY, cv::Point(0, 0), 0, cv::BORDER_REFLECT);
  } else if (mode == 2) {  // backward difference
    kernelX = (cv::Mat_<double>(1, 2) << -1, 1);
    kernelY = (cv::Mat_<double>(2, 1) << -1, 1);
    cv::filter2D(psi_, Ix, -1, kernelX, cv::Point(1, 0), 0, cv::BORDER_REFLECT);
    cv::filter2D(psi_, Iy, -1, kernelY, cv::Point(0, 1), 0, cv::BORDER_REFLECT);
  } else if (mode == 3) {  // first order central difference
    kernelX = (cv::Mat_<double>(1, 3) << -0.5, 0, 0.5);
    kernelY = (cv::Mat_<double>(3, 1) << -0.5, 0, 0.5);
    cv::filter2D(psi_, Ix, -1, kernelX, cv::Point(-1, -1), 0.2, cv::BORDER_REFLECT);
    cv::filter2D(psi_, Iy, -1, kernelY, cv::Point(-1, -1), 0.2, cv::BORDER_REFLECT);
  } else if (mode == 4) {  // second order central difference
    kernelX = (cv::Mat_<double>(1, 3) << 1, -2, 1);
    kernelY = (cv::Mat_<double>(3, 1) << 1, -2, 1);
    cv::filter2D(psi_, Ix, -1, kernelX, cv::Point(-1, -1), 0.2, cv::BORDER_REFLECT);
    cv::filter2D(psi_, Iy, -1, kernelY, cv::Point(-1, -1), 0.2, cv::BORDER_REFLECT);
  } else if (mode == 5) {  //
    kernelX = (cv::Mat_<double>(3, 3) << 0.25, 0, -0.25, 0, 0, 0, -0.25, 0, 0.25);
    cv::filter2D(psi_, Ix, -1, kernelX, cv::Point(-1, -1), 0.2, cv::BORDER_REFLECT);
    cv::filter2D(psi_, Iy, -1, kernelX, cv::Point(-1, -1), 0.2, cv::BORDER_REFLECT);
  } else {
    std::cout << "[Level set] Mode is wrong!";
  }
}

/// compute the first term of the PDE
void ImplicitActiveContour::computeFirstTerm(cv::Mat& output) {
  std::cout << "[Level Set] Computing the first term..." << std::endl;
  // compute all the gradients that we need
  cv::Mat psiX;
  cv::Mat psiY;
  cv::Mat psiXX;
  cv::Mat psiYY;
  cv::Mat psiXY;
  cv::Mat psiYX;
  computePsiGradient(psiX, psiY, 3);
  computePsiGradient(psiXX, psiYY, 4);
  computePsiGradient(psiXY, psiYX, 5);

  // compute
  inputImage_.copyTo(output);
  for (int r = 0; r < output.rows; r++) {
    for (int c = 0; c < output.cols; c++) {
      double tempX = psiX.at<double>(r, c);
      double tempY = psiY.at<double>(r, c);
      double tempXX = psiXX.at<double>(r, c);
      double tempYY = psiYY.at<double>(r, c);
      double tempXY = psiXY.at<double>(r, c);
      output.at<double>(r, c) = (cv::pow(tempY, 2) * tempXX - 2 * tempX * tempY * tempXY + cv::pow(tempX, 2) * tempYY) / (cv::pow(tempX, 2) + cv::pow(tempY, 2)) * extendedPhi_.at<double>(r, c);
    }
  }
  std::cout << "[Level Set] Computing the first term ends!" << std::endl;
}

/// compute the second term of the PDE
void ImplicitActiveContour::computeSecondTerm(cv::Mat& output) {
  std::cout << "[Level Set] Computing the second term..." << std::endl;
  // compute all the gradients that we need
  cv::Mat psiXP;
  cv::Mat psiYP;
  cv::Mat psiXM;
  cv::Mat psiYM;
  computePsiGradient(psiXP, psiYP, 1);
  computePsiGradient(psiXM, psiYM, 2);

  // compute
  inputImage_.copyTo(output);
  for (int r = 0; r < output.rows; r++) {
    for (int c = 0; c < output.cols; c++) {
      double aP = maxValue(extendedPhiGradientX_.at<double>(r, c), 0);
      double bP = maxValue(extendedPhiGradientY_.at<double>(r, c), 0);
      double aM = minValue(extendedPhiGradientX_.at<double>(r, c), 0);
      double bM = minValue(extendedPhiGradientY_.at<double>(r, c), 0);
      output.at<double>(r, c) =  (aP * psiXM.at<double>(r, c) + aM * psiXP.at<double>(r, c)) +
          (bP * psiYM.at<double>(r, c) + bM * psiYP.at<double>(r, c));
    }
  }
  std::cout << output << std::endl;
  std::cout << "[Level Set] Computing the second term ends!" << std::endl;
}

/// compute the third term of the PDE
void ImplicitActiveContour::computeThirdTerm(cv::Mat& output, double para) {
  std::cout << "[Level Set] Computing the third term..." << std::endl;
  // compute all the gradients that we need
  cv::Mat psiXP;  // forward difference in X
  cv::Mat psiYP;  // forward difference in Y
  cv::Mat psiXM;  // backward difference in X
  cv::Mat psiYM;  // backward difference in Y
  computePsiGradient(psiXP, psiYP, 1);
  computePsiGradient(psiXM, psiYM, 2);

  // compute
  inputImage_.copyTo(output);
  for (int r = 0; r < output.rows; r++) {
    for (int c = 0; c < output.cols; c++) {
      double tempXP = psiXP.at<double>(r, c);
      double tempYP = psiYP.at<double>(r, c);
      double tempXM = psiXM.at<double>(r, c);
      double tempYM = psiYM.at<double>(r, c);

      // prepare for the entropy scheme
      double temp1 = cv::pow(maxValue(tempXP, 0), 2);
      double temp2 = cv::pow(minValue(tempXM, 0), 2);
      double temp3 = cv::pow(maxValue(tempYP, 0), 2);
      double temp4 = cv::pow(minValue(tempYM, 0), 2);
      output.at<double>(r, c) = cv::sqrt(temp1 + temp2 + temp3 + temp4) * para * extendedPhi_.at<double>(r, c);
    }
  }
  std::cout << "[Level Set] Computing the third term ends" << std::endl;
}

/// return a bigger value
double ImplicitActiveContour::maxValue(double input1, double input2) {
  if (input1 > input2)
    return input1;
  else
    return input2;
}

/// return a smaller value
double ImplicitActiveContour::minValue(double input1, double input2) {
  if (input1 < input2)
    return input1;
  else
    return input2;
}

/// update psi and the whole scheme
void ImplicitActiveContour::updateLevelSet(double timeInterval, double lambda, double para) {
  std::cout << "[Level Set] Updating..." << std::endl;

  // update psi
  cv::Mat term1;
  cv::Mat term2;
  cv::Mat term3;
  computeFirstTerm(term1);
  computeSecondTerm(term2);
  computeThirdTerm(term3, para);
  cv::Mat delta = term1 + term2 + term3; //+ term2 + term3;
  psi_ = psi_ + delta * timeInterval;

  // update the zero level set
  std::cout << findZeroLevelSet() << std::endl;

  // update extension of Phi
  for (int r = 0; r < nearestIndex_.rows; r++) {
    for (int c = 0; c < nearestIndex_.cols; c++) {
      // find the shortest distance
      double shortestDist = 1e10;
      for (int k = 0; k < zeroLevelSet_.size(); k++) {
        cv::Point currentPosition = zeroLevelSet_.at(k);
        double currentDist = cv::sqrt(cv::pow(c - currentPosition.x, 2) + cv::pow(r - currentPosition.y, 2));
        if (currentDist < shortestDist)
          nearestIndex_.at<uint16_t>(r, c) = k;
      }
    }
  }
  PhiExtension();

  // show and save image
  saveImage();

  // next iteration
  currentIter_++;

  // save matrix
  saveMatrix("/home/yao/Data/snakes/iterations/psi.txt", psi_);
  saveZeroLevelSet("/home/yao/Data/snakes/iterations/zl.txt");

  std::cout << "[Level Set] Updating ends..." << std::endl;
}

/// find zero level set
int ImplicitActiveContour::findZeroLevelSet() {
  // clear stashed Psi
  stashedZeroLevelSet_.clear();

  cv::Mat open = cv::Mat::zeros(psi_.rows, psi_.cols, CV_8U);  // mask for visiting

  // check every row
  for (int r = 0; r < psi_.rows; r++) {
    cv::Mat row = psi_.row(r);
    cv::Mat mask;
    row.convertTo(mask, CV_8U);
    for (int i = 0; i < mask.cols; i++) {
      if (row.at<double>(0, i) < 0.0)
        mask.at<uchar>(0, i) = 255;
      else
        mask.at<uchar>(0, i) = 0;
    }

    for (int c = 1; c < psi_.cols; c++) {
      if (mask.at<uchar>(0, c) == 255 && mask.at<uchar>(0, c - 1) == 0) {
        if (open.at<uchar>(r, c) == 0 ) {
          stashedZeroLevelSet_.push_back(cv::Point(c, r));
          open.at<uchar>(r, c) = 255;
        }
        if (open.at<uchar>(r, c - 1) == 0 ) {
          stashedZeroLevelSet_.push_back(cv::Point(c - 1, r));
          open.at<uchar>(r, c - 1) = 255;
        }
      }
      if (mask.at<double>(0, c) == 255 && mask.at<double>(0, c + 1) == 0) {
        if (open.at<uchar>(r, c) == 0) {
          stashedZeroLevelSet_.push_back(cv::Point(c, r));
          open.at<uchar>(r, c) = 255;
        }
        if (open.at<uchar>(r, c + 1) == 0) {
          stashedZeroLevelSet_.push_back(cv::Point(c + 1, r));
          open.at<uchar>(r, c + 1) = 255;
        }
      }
    }
  }

  // check every column
  for (int c = 0; c < psi_.cols; c++) {
    cv::Mat col = psi_.col(c);
    cv::Mat mask;
    col.convertTo(mask, CV_8U);
    for (int i = 0; i < mask.rows; i++) {
      if (col.at<double>(i, 0) < 0.0)
        mask.at<uchar>(i, 0) = 255;
      else
        mask.at<uchar>(i, 0) = 0;
    }

    for (int r = 1; r < psi_.rows; r++) {
      if (mask.at<uchar>(r, 0) == 255 && mask.at<uchar>(r - 1, 0) == 0) {
        if (open.at<uchar>(r, c) == 0 ) {
          stashedZeroLevelSet_.push_back(cv::Point(c, r));
          open.at<uchar>(r, c) = 255;
        }
        if (open.at<uchar>(r - 1, c) == 0 ) {
          stashedZeroLevelSet_.push_back(cv::Point(c, r - 1));
          open.at<uchar>(r - 1, c) = 255;
        }
      }
      if (mask.at<uchar>(r, 0) == 255 && mask.at<uchar>(r + 1, 0) == 0) {
        if (open.at<uchar>(r, c) == 0 ) {
          stashedZeroLevelSet_.push_back(cv::Point(c, r));
          open.at<uchar>(r, c) = 255;
        }
        if (open.at<uchar>(r + 1, c) == 0 ) {
          stashedZeroLevelSet_.push_back(cv::Point(c, r + 1));
          open.at<uchar>(r + 1, c) = 255;
        }
      }
    }
  }

  zeroLevelSet_ = stashedZeroLevelSet_;
  return zeroLevelSet_.size();
}

/// save matrix
void ImplicitActiveContour::saveMatrix(const std::string filename, cv::Mat mat) {
  std::ofstream fout(filename.c_str(), std::ios::out);
  if(!fout) {
      std::cout << "[Level Set] File Not Opened!" << std::endl;
      exit(-1);
  }

  for(int r = 0; r < mat.rows; r++) {
    for(int c = 0; c < mat.cols; c++){
        fout << int(mat.at<double>(r, c)) << "\t";
    }
    fout << std::endl;
  }
  fout.close();
}

/// save zero level set
void ImplicitActiveContour::saveZeroLevelSet(const std::string filename) {
  std::ofstream fout(filename.c_str(), std::ios::out);
  if(!fout) {
      std::cout << "[Level Set] File Not Opened!" << std::endl;
      exit(-1);
  }

  for (int i = 0; i < zeroLevelSet_.size(); i++) {
    fout << zeroLevelSet_.at(i).x << " " << zeroLevelSet_.at(i).y << std::endl;
  }
  fout.close();
}

void ImplicitActiveContour::saveImage() {
  inputImage_.convertTo(image4Display_, CV_8U);
  cv::cvtColor(image4Display_, image4Display_, CV_GRAY2RGB);
  for (size_t i = 0; i < zeroLevelSet_.size(); i++) {
    cv::Point currentPoint = zeroLevelSet_.at(i);
    image4Display_.at<cv::Vec3b>(currentPoint.y, currentPoint.x)[0] = 0;
    image4Display_.at<cv::Vec3b>(currentPoint.y, currentPoint.x)[1] = 255;
    image4Display_.at<cv::Vec3b>(currentPoint.y, currentPoint.x)[2] = 0;
  }

  // save image
  const std::string outputFilename = outputDir_ + "Iteration-" + boost::lexical_cast<std::string>(currentIter_) + ".jpg";
  cv::imwrite(outputFilename, image4Display_);
}

/// get depth of all images
void ImplicitActiveContour::getDepth() const {
  std::cout << inputImage_.depth() << std::endl;
  std::cout << image4Display_.depth() << std::endl;
  std::cout << psi_.depth() << std::endl;
  std::cout << nearestIndex_.depth() << std::endl;
  std::cout << Phi_.depth() << std::endl;
  std::cout << PhiX_.depth() << std::endl;
  std::cout << PhiY_.depth() << std::endl;
  std::cout << extendedPhi_.depth() << std::endl;
  std::cout << extendedPhiGradientX_.depth() << std::endl;
  std::cout << extendedPhiGradientY_.depth() << std::endl;
}


/// set input image
void ImplicitActiveContour::setInputImage(cv::Mat image) {
  image.copyTo(inputImage_);
}

/// set output path
void ImplicitActiveContour::setOutputDir(const std::string outputDir) {
  outputDir_ = outputDir;
}
}


