clear all;
close all;
clc;

%% set up sigma
sigma = 0.2;

%% add noise
filename = '/home/yao/Data/snakes/testimages/test3-normal.jpg';
image = im2double(imread(filename));
noise = normrnd(0, sigma, size(image, 1), size(image, 2), 3);
noisyImage = image + noise;

%% save and display
imwrite(noisyImage, '/home/yao/Data/snakes/testimages/test3.jpg')
imshow(noisyImage)
