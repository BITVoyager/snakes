/*
 * ActiveContour.h
 *
 *  Created on: Apr 17, 2015
 *      Author: yao
 */

#include <opencv2/core/core.hpp>

namespace activecontour {

class ExplicitActiveContour {
private:
cv::Mat inputImage_;
cv::Mat image4Display_;

// samples
std::vector<cv::Point> samples_;
std::vector<cv::Point> stashedSamples_;
int minDist_;
int maxDist_;

// output path
std::string outputDir_;

// current iteration
size_t currentIter_;

public:
  // default constructor
  ExplicitActiveContour():
    outputDir_(""), currentIter_(1), minDist_(3), maxDist_(10){};

  // constructor

  // functional methods
  double initialize(cv::Point center, int radius, int resolution);
  cv::Mat computeTangent(size_t pointIndex, double deltaP);
  cv::Mat computeUnitNorm(size_t pointIndex, double deltaP);
  double computeCurvature(size_t pointIndex, double deltaP);
  void computeGradient(cv::Mat& Ix, cv::Mat& Iy, int mode);
  double measurementFunc(double Ix, double Iy, double para1);
  bool runActiveContour(size_t maxIter, double timeInterval, double deltaP, double para1);
  void resampling();
  void nextIter();
  void printSamples();
  double dotProduct(cv::Mat mat1, cv::Mat mat2);

  // get methods


  // set methods
  bool setInputImage(cv::Mat image);
  void setOutputDir(std::string outputDir);
};  // end of class ActiveContour

}  // end of namespace snakes
