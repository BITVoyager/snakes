/*
 * ImplicitActiveContour.h
 *
 *  Created on: Apr 22, 2015
 *      Author: yao
 */

#include <opencv2/core/core.hpp>

namespace activecontour {
class ImplicitActiveContour {
private:
cv::Mat inputImage_;
cv::Mat image4Display_;

cv::Mat psi_;
cv::Mat nearestIndex_;  // store the nearest point index in the zero level set
cv::Mat Phi_;   // extended Phi
cv::Mat PhiX_;  // X gradient of extended Phi
cv::Mat PhiY_;  // Y gradient of extended Phi
cv::Mat extendedPhi_;   // extended Phi
cv::Mat extendedPhiGradientX_;  // X gradient of extended Phi
cv::Mat extendedPhiGradientY_;  // Y gradient of extended Phi

// points of zero level set
std::vector<cv::Point> zeroLevelSet_;
std::vector<cv::Point> stashedZeroLevelSet_;

// output path
std::string outputDir_;

// current iteration
size_t currentIter_;

public:
  // default constructor
  ImplicitActiveContour():
    outputDir_(""), currentIter_(1){};

  // constructor

  // functional methods
  size_t initialize(cv::Point center, double radius, int resolution);

  void computeImageGradient(cv::Mat& Ix, cv::Mat& Iy, int mode);
  void PhiDescription(double lambda);
  void PhiExtension();

  void computePsiGradient(cv::Mat& Ix, cv::Mat& Iy, int mode);

  void computeFirstTerm(cv::Mat& output);
  void computeSecondTerm(cv::Mat& output);
  void computeThirdTerm(cv::Mat& output, double para);

  int findZeroLevelSet();
  void updateLevelSet(double timeInterval, double lambda, double para);

  double maxValue(double input1, double input2);
  double minValue(double input1, double input2);

  void saveMatrix(std::string filename, cv::Mat mat);
  void saveZeroLevelSet(const std::string filename);
  void saveImage();

  // get methods
  void getDepth() const;

  // set methods
  void setInputImage(cv::Mat image);
  void setOutputDir(std::string outputDir);
};  // end of class ImplicitActiveContour
}
