clear all;
close all;
clc;

%% initiliazation
radius = 30;
center = [100; 100];

image = ones(2 * center(2), 2 * center(1));
figure(1);
imshow(image, 'Border', 'tight');
rectangle('Position', [center(1) - radius, center(2) - radius, 2 * radius, 2 * radius], 'Curvature', [1,1], 'FaceColor','r', 'EdgeColor', 'r');