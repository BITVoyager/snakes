/*
 * ExplicitActiveContour.cpp
 *
 *  Created on: Apr 17, 2015
 *      Author: yao
 */

#include "ExplicitActiveContour.h"

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <boost/filesystem.hpp>
#include <boost/lexical_cast.hpp>

#include <vector>

#include <iostream>

namespace activecontour {

/// create a circle as the initial input
double ExplicitActiveContour::initialize(cv::Point center, int radius, int resolution) {

  // get points on a circle
  cv::Size axes(radius, radius);
  cv::ellipse2Poly(center, axes, 0, 0, 360, resolution, samples_);
  samples_.pop_back();  // remove the last element, which is the same with the first one

  // create an image for display
  inputImage_.convertTo(image4Display_, CV_8U);
  cv::cvtColor(image4Display_, image4Display_, CV_GRAY2RGB);
  for (size_t i = 0; i < samples_.size(); i++) {
    cv::Point currentPoint = samples_.at(i);
    image4Display_.at<cv::Vec3b>(currentPoint.y, currentPoint.x)[0] = 0;
    image4Display_.at<cv::Vec3b>(currentPoint.y, currentPoint.x)[1] = 0;
    image4Display_.at<cv::Vec3b>(currentPoint.y, currentPoint.x)[2] = 255;
  }

  // show image
  const std::string widowName = "Iteration No. " + boost::lexical_cast<std::string>(currentIter_);
  cv::imshow(widowName, image4Display_);
  cv::waitKey(10);

  // save image
  const std::string outputFilename = outputDir_ + "Iteration-" + boost::lexical_cast<std::string>(currentIter_) + ".jpg";
  cv::imwrite(outputFilename, image4Display_);

  // next iteration
  currentIter_++;

  return 2 * M_PI / samples_.size();
}

/// compute tangent vector Cp using central difference (clockwise)
cv::Mat ExplicitActiveContour::computeTangent(size_t pointIndex, double deltaP) {
  double dx;
  double dy;
  if (pointIndex == samples_.size() - 1) {
    dx = samples_.at(0).x - samples_.at(pointIndex - 1).x;
    dy = samples_.at(0).y - samples_.at(pointIndex - 1).y;
  } else if (pointIndex == 0) {
    dx = samples_.at(1).x - samples_.at(samples_.size() - 1).x;
    dy = samples_.at(1).y - samples_.at(samples_.size() - 1).y;
  } else {
    dx = samples_.at(pointIndex + 1).x - samples_.at(pointIndex - 1).x;
    dy = samples_.at(pointIndex + 1).y - samples_.at(pointIndex - 1).y;
  }
  cv::Mat tangent = (cv::Mat_<double>(2, 1) << dx / (2 * deltaP), dy / (2 * deltaP));
  return tangent;
}

/// compute the unit normal
cv::Mat ExplicitActiveContour::computeUnitNorm(size_t pointIndex, double deltaP) {
  cv::Mat tangent = computeTangent(pointIndex, deltaP);
  cv::Mat unitTangent = tangent / cv::norm(tangent);
  cv::Mat rotation = (cv::Mat_<double>(2, 2) << 0, -1, 1, 0);
  cv::Mat unitNorm = rotation * unitTangent;
  return unitNorm;
}

/// compute curvature
double ExplicitActiveContour::computeCurvature(size_t pointIndex, double deltaP) {
  // compute Cpp
  double ddx;
  double ddy;
  if (pointIndex == samples_.size() - 1) {
    ddx = samples_.at(0).x + samples_.at(pointIndex - 1).x - 2 * samples_.at(pointIndex).x;
    ddy = samples_.at(0).y + samples_.at(pointIndex - 1).y - 2 * samples_.at(pointIndex).y;
  } else if (pointIndex == 0) {
    ddx = samples_.at(1).x + samples_.at(samples_.size() - 1).x - 2 * samples_.at(pointIndex).x;
    ddy = samples_.at(1).y + samples_.at(samples_.size() - 1).y - 2 * samples_.at(pointIndex).y;
  } else {
    ddx = samples_.at(pointIndex + 1).x + samples_.at(pointIndex - 1).x - 2 * samples_.at(pointIndex).x;
    ddy = samples_.at(pointIndex + 1).y + samples_.at(pointIndex - 1).y - 2 * samples_.at(pointIndex).y;
  }
  cv::Mat cpp = (cv::Mat_<double>(2, 1) << ddx / cv::pow(deltaP, 2), ddy / cv::pow(deltaP, 2));

  // compute unit norm
  cv::Mat unitNorm = computeUnitNorm(pointIndex, deltaP);

  // compute Cp
  cv::Mat tangent = computeTangent(pointIndex, deltaP);
  double cpNorm = cv::norm(tangent);

  // compute curvature
  double curvature = dotProduct(cpp, unitNorm) / cv::pow(cpNorm, 2);
  return curvature;
}

/// compute gradient: mode = 1, forward; mode = 2, backward; mode = 3, central
void ExplicitActiveContour::computeGradient(cv::Mat& Ix, cv::Mat& Iy, int mode) {
  cv::Mat kernelX;
  cv::Mat kernelY;

  if (mode == 1) {  // forward difference
    kernelX = (cv::Mat_<double>(1, 2) << -1, 1);
    kernelY = (cv::Mat_<double>(2, 1) << -1, 1);
    cv::filter2D(inputImage_, Ix, -1, kernelX, cv::Point(0, 0), 0, cv::BORDER_REFLECT);
    cv::filter2D(inputImage_, Iy, -1, kernelY, cv::Point(0, 0), 0, cv::BORDER_REFLECT);
  } else if (mode == 2) {  // backward difference
    kernelX = (cv::Mat_<double>(1, 2) << -1, 1);
    kernelY = (cv::Mat_<double>(2, 1) << -1, 1);
    cv::filter2D(inputImage_, Ix, -1, kernelX, cv::Point(1, 0), 0, cv::BORDER_REFLECT);
    cv::filter2D(inputImage_, Iy, -1, kernelY, cv::Point(0, 1), 0, cv::BORDER_REFLECT);
  } else if (mode == 3) {  // central difference
    kernelX = (cv::Mat_<double>(1, 3) << -0.5, 0, 0.5);
    kernelY = (cv::Mat_<double>(3, 1) << -0.5, 0, 0.5);
    cv::filter2D(inputImage_, Ix, -1, kernelX, cv::Point(-1, -1), 0, cv::BORDER_REFLECT);
    cv::filter2D(inputImage_, Iy, -1, kernelY, cv::Point(-1, -1), 0, cv::BORDER_REFLECT);
  } else {
    std::cout << "[Active Contour] Mode is wrong!";
  }
}

/// comute the output of the measurement function
double ExplicitActiveContour::measurementFunc(double Ix, double Iy, double para1) {
  double value = cv::pow(para1, 2) / (cv::pow(para1, 2) + cv::pow(Ix, 2) + cv::pow(Iy, 2));
  return value;
}

bool ExplicitActiveContour::runActiveContour(size_t maxIter, double timeInterval, double deltaP, double para1) {
  // compute the gradient images
  cv::Mat imageIx;
  cv::Mat imageIy;
  computeGradient(imageIx, imageIy, 1);

  // loop for every iteration
  for (size_t iter = 0; iter < maxIter; iter++) {

    // resample if needed
    resampling();

    for (size_t sampleIndex = 0; sampleIndex < samples_.size(); sampleIndex++) {
      // get the position of the sample
      cv::Point position = samples_.at(sampleIndex);

      // get the unit normal and curvature
      double curvature = computeCurvature(sampleIndex, deltaP);
      cv::Mat unitNorm = computeUnitNorm(sampleIndex, deltaP);

      // compute gradient of Phi
      // TODO: check the CFL condition
      double Ix = imageIx.at<double>(position.y, position.x);
      double Iy = imageIx.at<double>(position.y, position.x);
      double Ixp1 = imageIx.at<double>(position.y, position.x + 1);
      double Iyp1 = imageIx.at<double>(position.y + 1, position.x);
      double Ixm1 = imageIx.at<double>(position.y, position.x - 1);
      double Iym1 = imageIx.at<double>(position.y - 1, position.x);
      double Phixp1 = measurementFunc(Ixp1, Iy, 200.0);
      double Phixm1 = measurementFunc(Ixm1, Iy, 200.0);
      double Phiyp1 = measurementFunc(Ix, Iyp1, 200.0);
      double Phiym1 = measurementFunc(Ix, Iym1, 200.0);
      cv::Mat gradientPhi = (cv::Mat_<double>(2, 1) << (Phixp1 - Phixm1) / 2, (Phiyp1 - Phiym1) / 2);
      std::cout << "The gradient of Phi is " << gradientPhi << std::endl;

      // compute the increment along the unit normal
      double increment = measurementFunc(Ix, Iy, 200.0) * (curvature + para1) - dotProduct(gradientPhi, unitNorm);

      // compute the new position
      int incrementX  = (unitNorm.at<double>(0, 0) * increment * timeInterval);
      int incrementY  = (unitNorm.at<double>(1, 0) * increment * timeInterval);
      std::cout << "The x increment is " << unitNorm.at<double>(0, 0) * increment << std::endl;
      std::cout << "The y increment is " << unitNorm.at<double>(1, 0) * increment << std::endl;
      stashedSamples_.push_back(cv::Point_<int>(position.x + incrementX, position.y + incrementY));
    }

    // create an image for display
    inputImage_.convertTo(image4Display_, CV_8U);
    cv::cvtColor(image4Display_, image4Display_, CV_GRAY2RGB);
    for (size_t i = 0; i < stashedSamples_.size(); i++) {
      cv::Point currentPoint = stashedSamples_.at(i);
      image4Display_.at<cv::Vec3b>(currentPoint.y, currentPoint.x)[0] = 0;
      image4Display_.at<cv::Vec3b>(currentPoint.y, currentPoint.x)[1] = 0;
      image4Display_.at<cv::Vec3b>(currentPoint.y, currentPoint.x)[2] = 255;
    }

    // save results
    const std::string outputFilename = outputDir_ + "Iteration-" + boost::lexical_cast<std::string>(currentIter_) + ".jpg";
    cv::imwrite(outputFilename, image4Display_);

    // next iteration
    nextIter();
  }
  return true;
}

/// resample the points if needed
void ExplicitActiveContour::resampling() {

}

/// next iteration
void ExplicitActiveContour::nextIter() {
  currentIter_ ++;  // next iteration
  samples_ = stashedSamples_;
  stashedSamples_.clear();  // clear stashedSamples_
}

/// compute dot product of two vector in mat format
double ExplicitActiveContour::dotProduct(cv::Mat mat1, cv::Mat mat2) {
  double value = cv::sum(mat1.mul(mat2))[0];
  return value;
}

/// set input image
bool ExplicitActiveContour::setInputImage(cv::Mat image) {
  image.copyTo(inputImage_);
  return true;
}

/// set output path
void ExplicitActiveContour::setOutputDir(const std::string outputDir) {
  outputDir_ = outputDir;
}
} // end of namespace snakes


