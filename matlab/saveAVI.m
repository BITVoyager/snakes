frameRate = 30;
quality = 75;
inputDir = '/home/yao/Data/snakes/testimages/blurredImage/';
imagesDir = dir(inputDir);
images = cell(size(imagesDir, 1) - 2, 1);

for k = 3: size(imagesDir, 1)
    filename = fullfile(inputDir, imagesDir(k).name);
    img = imread(filename);
    images{k - 2, 1} = img;
end

[nFrames, ~] = size(images);
mov(1:nFrames) = struct('cdata', [], 'colormap', []);

% create movie
for k = 1:nFrames
    image  = images{k};
    mov(k) = im2frame(image);
end

% save as AVI file, see http://www.mathworks.com/help/matlab/ref/movie2avi.html
movie2avi(mov, strcat(inputDir, 'result.avi'));