/*
 * DataLoader.h

 *
 *  Created on: Apr 6, 2015
 *      Author: Yao Chen
 *      Brief: load data
 *
 */

#include <opencv2/core/core.hpp>

namespace activecontour {

/// class DataLoader
class DataLoader {
private:
  // for image directories
  const std::string datasetDir_;  // image directory
  const std::string imageFilenamePre_;  // image filename prefix
  const std::string imageFilenameExt_;  // image filename extension

  // images
  cv::Mat originalImage_;   // original gray-scale image in CV_64FC1
  cv::Mat blurredImage_;    // blurred gray-scale image in CV_64FC1
  cv::Mat prevImage_;

  // Energy
  std::vector<double> energy_;

  // stop flag;
  double stopFlag_;

public:
  // default constructor
  DataLoader():
    datasetDir_(""), imageFilenamePre_(""), imageFilenameExt_(""), stopFlag_(90.0){};

  // constructor
  DataLoader(std::string datasetDir, std::string imageFilenamePre, std::string imageFilenameExt, double stopFlag):
    datasetDir_(datasetDir), imageFilenamePre_(imageFilenamePre), imageFilenameExt_(imageFilenameExt), stopFlag_(stopFlag) {};

  // functional methods
  bool loadImage();  // load image
  bool blurImageGaussian(cv::Size kSize, double sigmaX, double sigmaY);  // blur image using standard smoothing method
  bool blurImageHeatEquation(size_t nrIterations, double timeInterval);  // blur image using 2D heat equation
  bool blurImageNonlinearDiff(size_t nrIterations, double timeInterval, double lambda, double gamma);  // blur image using 2D heat equation
  double gDescription(double gradient, double lambda);
  double getPSNR(const cv::Mat I1, const cv::Mat I2);
  std::string leading_zeros(size_t num, size_t length);  // leading zeros
  double energyFunction(double lambda, const cv::Mat gX, const cv::Mat gY);
  bool saveEnergy(std::string filename);

  // get methods
  cv::Mat getOriginalImage() const { return originalImage_; }
  cv::Mat getBlurredImage() const { return blurredImage_; }

  // set methods
  void setStopFlag(double stopFlag);

};  // end of class DataLoader

}  // end of namespace snakes
