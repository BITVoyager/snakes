/*
 * Yao03_ImageGradient.cpp
 *
 *  Created on: Apr 20, 2015
 *      Author: yao
 */
#include "ImplicitActiveContour.h"
#include "DataLoader.h"

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <boost/filesystem.hpp>
#include <boost/lexical_cast.hpp>

#include <cmath>

#include <sstream>
#include <iomanip>

using namespace std;
using namespace activecontour;

int main(int argc, char* argv[]) {
  string dataDir = "/home/yao/Data/snakes/testimages/";
  string imageFilenamePre = "test9";
  string imageFilenameExt = ".png";

  DataLoader loader(dataDir, imageFilenamePre, imageFilenameExt, 90);
  loader.loadImage();

  ImplicitActiveContour levelSet;
  levelSet.setInputImage(loader.getOriginalImage());
  levelSet.setOutputDir("/home/yao/Data/snakes/iterations/");

  cv::Point center(200 , 200);
  levelSet.initialize(center, 40.2, 5);
  levelSet.PhiDescription(201);
  levelSet.PhiExtension();

  for (int i = 0; i < 200; i++)
    levelSet.updateLevelSet(0.1, 1, 5);

  return 0;
}
