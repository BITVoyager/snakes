/*
 * DataLoader.cpp
 *
 *  Created on: Apr 6, 2015
 *      Author: yao
 */
#include "DataLoader.h"

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <boost/filesystem.hpp>
#include <boost/lexical_cast.hpp>

#include <cmath>

#include <sstream>
#include <iomanip>
#include <fstream>
#include <iostream>

namespace activecontour {

/// load image
bool DataLoader::loadImage() {
  // check whether the directory is really a directory
  if (!boost::filesystem::is_directory(datasetDir_)) {
    std::cout << "[DataLoader] Directory Error: Not a directory!" << std::endl;
    exit(-1);
  }

  // check whether the file exists
  std::string filename = datasetDir_ + imageFilenamePre_ + imageFilenameExt_;
  if (!boost::filesystem::exists(filename)) {
    std::cout << "[DataLoader] Path Error: Path does not exist!" << std::endl;
    exit(-1);
  }

  // load image and convert it into gray-scale if it is a color image
  cv::Mat temp = cv::imread(filename, cv::IMREAD_UNCHANGED);
  if (temp.channels() != 3 && temp.channels() != 1) {
    std::cout << "[DataLoader] Image Error: Wrong Image channels!" << std::endl;
    exit(-1);
  }
  else {
    if (temp.channels() == 3) {
      std::cout << "[DataLoader] Converting color image to gray-scale..." << std::endl;
      cv::cvtColor(temp, originalImage_, CV_RGB2GRAY);
      originalImage_.convertTo(originalImage_, CV_64FC1);
    }
    else {
      std::cout << "[DataLoader] Copying gray-scale image..." << std::endl;
      temp.convertTo(temp, CV_64FC1);
      temp.copyTo(originalImage_);
    }
  }
  std::cout << "[DataLoader] Loading image down!" << std::endl;
  return true;
}

/// blur image with Gaussian kernel
bool DataLoader::blurImageGaussian(cv::Size kSize, double sigmaX, double sigmaY) {
  std::cout << "[DataLoader] Blurring image with Gaussian kernel..." << std::endl;
  if (sigmaX == sigmaY)
    cv::GaussianBlur(originalImage_, blurredImage_, kSize, sigmaX, 0, cv::BORDER_REFLECT);
  else
    cv::GaussianBlur(originalImage_, blurredImage_, kSize, sigmaX, sigmaY, cv::BORDER_REFLECT);
  std::cout << "[DataLoader] Blurring image down!" << std::endl;
  return true;
}

/// blur image with linear heat equation
bool DataLoader::blurImageHeatEquation(size_t nrIterations, double timeInterval) {

  std::cout << "[DataLoader] Blurring image with heat equation..." << std::endl;

  // create a padded image
  int borderWidth = 1;
  cv::Mat paddedImage;
  cv::copyMakeBorder(originalImage_, paddedImage, borderWidth, borderWidth, borderWidth, borderWidth, cv::BORDER_REFLECT);

  // create a blurred image
  cv::Mat blurredImage;
  originalImage_.copyTo(blurredImage);

  // set up output path
  std::string outputDir = datasetDir_ + "blurredImage/";
  if (!boost::filesystem::exists(outputDir)) {
    boost::filesystem::path boost_path(outputDir);
    boost::filesystem::create_directory(boost_path);
  }

  // blurring
  for (size_t iter = 1; iter <= nrIterations; iter++) {

    for (int r = 1; r < paddedImage.rows - 1; r++) {
      for (int c = 1; c < paddedImage.cols - 1; c++) {
        double temp = paddedImage.at<double>(r, c) * (1 - 4 * timeInterval) + (paddedImage.at<double>(r - 1, c) +
            paddedImage.at<double>(r + 1, c) + paddedImage.at<double>(r, c - 1) + paddedImage.at<double>(r, c + 1)) * timeInterval;
        blurredImage.at<double>(r - 1, c - 1) = temp;
      }
    }

    // pad the blurred image for the next iteration
    cv::copyMakeBorder(blurredImage, paddedImage, borderWidth, borderWidth, borderWidth, borderWidth, cv::BORDER_REFLECT);

    // save results after iteration
    std::string outputFilename = outputDir + "Blurred-" + leading_zeros(iter, 8) + ".jpg";
    cv::Mat image4Display;
    blurredImage.convertTo(image4Display, CV_8U);
    cv::cvtColor(image4Display, image4Display, CV_GRAY2RGB);
    cv::imwrite(outputFilename, image4Display);
  }
  blurredImage.copyTo(blurredImage_);
  std::cout << "[DataLoader] Blurring image down!" << std::endl;
  return true;
}

/// blur image with gradient descent
bool DataLoader::blurImageNonlinearDiff(size_t maxIter, double timeInterval, double lambda, double gamma) {
  std::cout << "[DataLoader] Blurring image with gradient descent..." << std::endl;

  // create a blurred image
  cv::Mat blurredImage;
  originalImage_.copyTo(blurredImage);

  // create a temporary image
  cv::Mat temp;
  originalImage_.copyTo(temp);

  // initialize the previous image
  originalImage_.copyTo(prevImage_);

  // create the gradient images in both X and Y directions (forward and backward difference schemes)
  cv::Mat kernelX = (cv::Mat_<double>(1, 2) << -1, 1);
  cv::Mat kernelY = (cv::Mat_<double>(2, 1) << -1, 1);
  cv::Mat fowardGradientX;
  cv::Mat fowardGradientY;
  cv::Mat backwardGradientX;
  cv::Mat backwardGradientY;
  cv::filter2D(temp, fowardGradientX, -1, kernelX, cv::Point(0, 0), 0, cv::BORDER_REFLECT);
  cv::filter2D(temp, fowardGradientY, -1, kernelY, cv::Point(0, 0), 0, cv::BORDER_REFLECT);
  cv::filter2D(temp, backwardGradientX, -1, kernelX, cv::Point(1, 0), 0, cv::BORDER_REFLECT);
  cv::filter2D(temp, backwardGradientY, -1, kernelY, cv::Point(0, 1), 0, cv::BORDER_REFLECT);

  // set up output path
  std::string outputDir = datasetDir_ + "blurredImage/";
  if (!boost::filesystem::exists(outputDir)) {
    boost::filesystem::path boost_path(outputDir);
    boost::filesystem::create_directory(boost_path);
  }

  // blurring
  for (size_t iter = 0; iter < maxIter; iter++) {

    for (int r = 0; r < temp.rows; r++) {
      for (int c = 0; c < temp.cols; c++) {

        // get gradient values
        double forwardIx = fowardGradientX.at<double>(r, c);
        double backwardIx = backwardGradientX.at<double>(r, c);
        double forwardIy = fowardGradientY.at<double>(r, c);
        double backwardIy = backwardGradientY.at<double>(r, c);

        // get intensity
        double I = temp.at<double>(r, c);
        double I0 = originalImage_.at<double>(r, c);

        // compute value of the blurred image
        double temp1 = gDescription(backwardIx, lambda) * (-backwardIx);
        double temp2 = gDescription(forwardIx, lambda) * (forwardIx);
        double temp3 = gDescription(backwardIy, lambda) * (-backwardIy);
        double temp4 = gDescription(forwardIy, lambda) * (forwardIy);
        blurredImage.at<double>(r, c) = (1 - gamma) * timeInterval * (temp1 + temp2 + temp3 + temp4)
            + (1 - 2 * gamma) * I + gamma * I0;
      }
    }

    // compute energy
    double energy = energyFunction(lambda, fowardGradientX, fowardGradientY);
    energy_.push_back(energy);

    // compute similarity
    if (getPSNR(blurredImage, prevImage_) > stopFlag_) {
      blurredImage.copyTo(blurredImage_);
      break;
    }

    // pad the blurred image for the next iteration
    blurredImage.copyTo(temp);
    cv::filter2D(temp, fowardGradientX, -1, kernelX, cv::Point(0, 0), 0, cv::BORDER_REFLECT);
    cv::filter2D(temp, fowardGradientY, -1, kernelY, cv::Point(0, 0), 0, cv::BORDER_REFLECT);
    cv::filter2D(temp, backwardGradientX, -1, kernelX, cv::Point(1, 0), 0, cv::BORDER_REFLECT);
    cv::filter2D(temp, backwardGradientY, -1, kernelY, cv::Point(0, 1), 0, cv::BORDER_REFLECT);
    blurredImage.copyTo(prevImage_);

    // save results after iteration
    std::string outputFilename = outputDir + "Blurred-" + leading_zeros(iter, 8) + ".jpg";
    cv::Mat image4Display;
    blurredImage.convertTo(image4Display, CV_8U);
    cv::cvtColor(image4Display, image4Display, CV_GRAY2RGB);
    cv::imwrite(outputFilename, image4Display);
  }

  blurredImage.copyTo(blurredImage_);

  return true;
}

/// description of g(x,y)
double DataLoader::gDescription(double gradient, double lambda) {
  double output = cv::pow(lambda, 2) / (cv::pow(lambda, 2) + cv::pow(gradient, 2));
  return output;
}

/// compute the energy
double DataLoader::energyFunction(double lambda, const cv::Mat gX, const cv::Mat gY) {
  // get the ROI i.e. gradient image without the one-pixel length padding
  cv::Mat gradientX;
  cv::Mat gradientY;
  cv::Mat gXROI = gX(cv::Rect(1, 1, gX.cols - 2, gX.rows - 2));
  cv::Mat gYROI = gX(cv::Rect(1, 1, gY.cols - 2, gY.rows - 2));
  gXROI.copyTo(gradientX);
  gYROI.copyTo(gradientY);

  // compute the energy
  cv::Mat Ix2;
  cv::Mat Iy2;
  cv::pow(gradientX, 2, Ix2);
  cv::pow(gradientY, 2, Iy2);

  cv::Mat sumTemp = Ix2 + Iy2 + cv::pow(lambda, 2) * cv::Mat::ones(gradientX.rows, gradientX.cols, CV_64FC1);
  cv::Mat logTemp;
  cv::log(sumTemp, logTemp);
  logTemp = logTemp * cv::pow(lambda, 2) / 2;
  double energy = cv::sum(logTemp)[0]; // / (cv::pow(255, 2) * gradientX.rows * gradientX.cols);
  return energy;
}

/// save energy information into a txt file for matlab
bool DataLoader::saveEnergy(std::string filename) {
  std::ofstream os(filename.c_str(), std::ios::out);
  for (int i = 0; i < energy_.size(); i++) {
    os << i << " " << energy_.at(i) << std::endl;
  }
  os.close();
  return true;
}

double DataLoader::getPSNR(const cv::Mat I1, const cv::Mat I2) {
 cv::Mat s1;
 cv::absdiff(I1, I2, s1);
 s1 = s1.mul(s1);

 double sse = cv::sum(s1)[0]; // sum channels

 if( sse <= 1e-9) // for small values return zero
   return 0;
 else {
   double mse = sse / (double)(I1.channels() * I1.total());
   double psnr = 10.0 * std::log10((255.0 * 255.0) / mse);
   return psnr;
 }
}

/// leading zeros
std::string DataLoader::leading_zeros(size_t num, size_t length) {
  std::ostringstream ss;
  ss << std::setw(length) << std::setfill('0') << num;
  return ss.str();
}

/// set stop flag
void DataLoader::setStopFlag(double stopFlag) {
  stopFlag_ = stopFlag;
}


} // end of namespace snakes



